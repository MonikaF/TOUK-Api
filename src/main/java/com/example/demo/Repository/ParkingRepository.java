package com.example.demo.Repository;

import com.example.demo.Model.Parking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Mońka on 2018-06-10.
 */
@Repository
public interface ParkingRepository extends JpaRepository<Parking,Long> {
    Parking findOneById(Long id);
    Parking findOneByUserId(Long id);

}
