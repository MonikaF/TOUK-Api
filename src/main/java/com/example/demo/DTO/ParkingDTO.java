package com.example.demo.DTO;

import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Mońka on 2018-06-10.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class ParkingDTO {

    private Long id;
    private Long userId;
    private Date startDate;
    private double payment;
    private Boolean isVIP;



}
