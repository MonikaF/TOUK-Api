package com.example.demo.Service;

import com.example.demo.DTO.ParkingDTO;
import com.example.demo.Model.Parking;
import com.example.demo.Repository.ParkingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Mońka on 2018-06-10.
 */
@Service
public class ParkingService {
    private final ParkingRepository parkingRepository;

    @Autowired
    public ParkingService(ParkingRepository parkingRepository) {
        this.parkingRepository = parkingRepository;
    }


    public Parking startParking(ParkingDTO parkingDTO){
        Parking parking = new Parking();
        parking.setUserId(parkingDTO.getUserId());
        parking.setStartDate(new Date());
        this.parkingRepository.save(parking);
        return parking;
    }

    public double stopParking(ParkingDTO parkingDTO){
        Parking parking = parkingRepository.findOneByUserId(parkingDTO.getUserId());
        Date stopDate =new Date();
        double payment = getPayment(parking.getStartDate(),stopDate, parkingDTO.getIsVIP());
        this.parkingRepository.delete(parking);
        return payment;
    }
    public double checkParking(ParkingDTO parkingDTO){
        Parking parking = parkingRepository.findOneByUserId(parkingDTO.getUserId());
        Date stopDate =new Date();
        double payment = getPayment(parking.getStartDate(),stopDate, parkingDTO.getIsVIP());

        return payment;
    }

    private double getPayment(Date startDate, Date stopDate, Boolean isVIP) {

        final int MILLI_TO_HOUR = 1000 * 60 * 60;
        double duration = (stopDate.getTime() - startDate.getTime()) / MILLI_TO_HOUR;
        double payment = 0;


        if(isVIP == true){
            if(duration<=1)
                payment = 0;

            if (duration<=2 && duration>1)
                payment=1;

            else if(duration>2)
                payment = 1;
                for(int i=0; i<duration-2;i++){
                    payment = 1.2 * payment;

                }
         }

        if(isVIP!=true){
            if(duration< 1)
                payment = 1;

            if (duration<=2 && duration>1)
                payment=2;

            else if(duration>2)
                payment = 2;
                for(int i=0; i<duration-2;i++){
                    payment = 1.5 * payment;

                }
        }


        return payment;
    }


    }
