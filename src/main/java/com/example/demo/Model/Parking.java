package com.example.demo.Model;

import com.example.demo.DTO.ParkingDTO;
import lombok.*;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Mońka on 2018-06-10.
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Parking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private Long id;

    @Column(unique = true)
    private Long userId;

    private Date startDate;

    public ParkingDTO toDTO(){
       ParkingDTO parkingDTO = new ParkingDTO();
       parkingDTO.setStartDate(this.startDate);
       parkingDTO.setId(this.id);

        return parkingDTO;
    }
}
