package com.example.demo.Controller;

import com.example.demo.DTO.ParkingDTO;
import com.example.demo.Model.Parking;
import com.example.demo.Service.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Created by Mońka on 2018-06-10.
 */
@RestController
public class ParkingController {
    private final ParkingService parkingService;

    @Autowired
    public ParkingController(ParkingService parkingService) {
        this.parkingService = parkingService;
    }


    @RequestMapping(value = "parking/stop",method = RequestMethod.POST)
    public ParkingDTO stopParking(ParkingDTO parkingDTO){
        double payment = parkingService.stopParking(parkingDTO);
        ParkingDTO responceDTO = new ParkingDTO();
        responceDTO.setPayment(payment);
        return responceDTO;
    }

    @RequestMapping(value = "parking/start",method = RequestMethod.POST)
    public ParkingDTO startParking(ParkingDTO parkingDTO){
        Parking parking = parkingService.startParking(parkingDTO);
        return parking.toDTO();

    }

    @RequestMapping(value = "parking/check",method = RequestMethod.POST)
    public  ParkingDTO checkParking (ParkingDTO parkingDTO){
        double payment = parkingService.checkParking(parkingDTO);
        ParkingDTO responceDTO = new ParkingDTO();
        responceDTO.setPayment(payment);
        return responceDTO;
    }
}


